// alert("howdy ")
// Activity Template:
// (Copy this part on your template)


/*
	1.) Create a function that returns a passed string with letters in alphabetical order.
		Example string: 'mastermind'
		Expected output: 'adeimmnrst'
	
*/

// Code here:
function alphabetizeOrder(string) {
	return string.split("").sort().join("");
};

console.log(alphabetizeOrder("mastermind")); //adeimmnrst

/*
	2.) Write a simple JavaScript program to join all elements of the following array into a string.

	Sample array : myColor = ["Red", "Green", "White", "Black"];
		Expected output: 
			Red,Green,White,Black
			Red,Green,White,Black
			Red+Green+White+Black

*/

// Code here:
function joinElements(array) {
	console.log(array.toString());
	console.log(array.join());
	console.log(array.join('+'));
}
let myColor = ["Red", "Green", "White", "Black"];
joinElements(myColor);
/*
	3.) Write a function named birthdayGift that pass a gift as an argument.
		- if the gift is a stuffed toy, return a message that says: "Thank you for the stuffed toy, Michael!"
		- if the gift is a doll, return a message that says: "Thank you for the doll, Sarah!"
		- if the gift is a cake, return a message that says: "Thank you for the cake, Donna!"
		- if other gifts return a message that says: "Thank you for the (gift), Dad!"
		- create a global variable named myGift and invoke the function in the variable.
		- log the global var in the console

*/

// Code here:
function birthdayGift(gift){
	if (gift == "stuffed toy"){
		console.log("Thank you for the stuffed toy, Michael!");
	}
	else if (gift == "doll"){
		console.log("Thank you for the doll, Sarah!");
	}
	else if (gift == "cake"){
		console.log("Thank you for the stuffed cake, Donna!");
	} else {
		console.log("Thank you for the " + gift + ", Dad!");
	};
};

let myGift = "stuffed toy";
birthdayGift(myGift);
myGift = "doll";
birthdayGift(myGift);
myGift = "cake";
birthdayGift(myGift);
myGift = "toothpick";
birthdayGift(myGift);

/*
	4.) Write a function that accepts a string as a parameter and counts the number of vowels within the string.

		Example string: 'The quick brown fox'
		Expected Output: 5

*/

// Code here:
const vowels = "aeiouAEIOU"; 
function countsVowels(string) {
	let count = 0;
	for (let i = 0; i < vowels.length; i++) {
		if(vowels.includes(string[i])) {
			count++;
		};
	};
	return console.log(count);
};
countsVowels("The quick brown fox");


